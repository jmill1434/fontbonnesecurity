package edu.fontbonne.muellerj.fontbonnesecurity2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;


public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "linksManager";
    private static final String TABLE_LINKS = "links";
    private static final String KEY_LINKS = "link";
    private static final String KEY_ID = "id";

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_LINKS_TABLE = "CREATE TABLE " + TABLE_LINKS + "(" + KEY_ID +
                " INTEGER PRIMARY KEY," + KEY_LINKS +
                " TEXT" + ")";
        db.execSQL(CREATE_LINKS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LINKS);
        onCreate(db);
    }

    void addLink(Link link){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_LINKS, link.getLink());

        db.insert(TABLE_LINKS, null, values);
        db.close();

    }

    Link getLink(int id){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_LINKS, new String[]{KEY_ID, KEY_LINKS}, KEY_ID + "w?",
        new String[]{String.valueOf(id)}, null, null, null, null);

        if(cursor != null){
            cursor.moveToFirst();
        }

        Link link = new Link(Integer.parseInt(cursor.getString(0)), cursor.getString(1));

        return link;

    }

    public List<Link> getAllLinks(){
        List<Link> linkList = new ArrayList<>();

        String selectQuery = "SELECT *FROM " + TABLE_LINKS;

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()){
            do{
                Link link = new Link();
                link.setId(Integer.parseInt(cursor.getString(0)));
                link.setLink(cursor.getString(1));

                linkList.add(link);

            } while(cursor.moveToNext());
        }
        return linkList;
    }

}

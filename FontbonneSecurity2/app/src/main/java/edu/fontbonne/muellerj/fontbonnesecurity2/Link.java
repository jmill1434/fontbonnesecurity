package edu.fontbonne.muellerj.fontbonnesecurity2;

public class Link {
    String link;
    int id;

    public Link(){

    }

    public Link(String link){
        this.link = link;
    }
    public Link(int id, String link){
        this.id = id;
        this.link = link;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

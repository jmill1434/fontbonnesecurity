package edu.fontbonne.muellerj.fontbonnesecurity2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class Main2Activity extends AppCompatActivity {

    WebView webView;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        url = getIntent().getStringExtra("urlString");

        webView = (WebView) findViewById(R.id.wv);
        webView.setWebViewClient(new WebViewClient());

        webView.loadUrl(url);
    }
}

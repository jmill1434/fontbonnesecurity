package edu.fontbonne.muellerj.fontbonnesecurity2;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;

import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    List<Link> arrayList;
    String link;
    Uri uri;
    String host;
    Intent intent;
    TextView textView;
    Button btn, btn2, btn3;
    int onList;

    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        db = new DatabaseHelper(this);


        onList = 0;

        btn = (Button) findViewById(R.id.btn);
        btn.setText("Check link");

        btn2 = (Button) findViewById(R.id.btn2);
        btn2.setText("Go to link");

        btn3 = (Button) findViewById(R.id.btn3);
        btn3.setText("Add to list");

        textView = (TextView) findViewById(R.id.tv);
        textView.setText("Hello World");

        String[] list = getResources().getStringArray(R.array.whiteList);


        for(int i = 0; i < list.length; i++){
            db.addLink(new Link(list[i]));
        }

        arrayList = db.getAllLinks();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = getIntent();
                link = intent.getDataString();
                uri = Uri.parse(link);
                host = uri.getHost();

                if(!host.startsWith("www.")){
                    host = "www." + host;
                }

                Log.i("host", host);


                for(int i = 0; i < arrayList.size(); i++){
                    if(host.equals(Uri.parse(arrayList.get(i).getLink()).getHost())){
                        textView.setText(host + " is on the list");
                        onList = 1;
                    }

                }
                if(onList == 0){
                    textView.setText(host + " is NOT on the list");
                }
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent webIntent = new Intent(MainActivity.this, Main2Activity.class);
                webIntent.putExtra("urlString", link);
                MainActivity.this.startActivity(webIntent);

            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                arrayList.add(new Link(link));
                db.addLink(new Link(link));
            }
        });







    }

}
